/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DetektorNaruszeniaBezpieczenstwa
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/DetektorNaruszeniaBezpieczenstwa.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "DetektorNaruszeniaBezpieczenstwa.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_DetektorNaruszeniaBezpieczenstwa_DetektorNaruszeniaBezpieczenstwa_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class DetektorNaruszeniaBezpieczenstwa
DetektorNaruszeniaBezpieczenstwa::DetektorNaruszeniaBezpieczenstwa(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(DetektorNaruszeniaBezpieczenstwa, DetektorNaruszeniaBezpieczenstwa(), 0, Default_DetektorNaruszeniaBezpieczenstwa_DetektorNaruszeniaBezpieczenstwa_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
}

DetektorNaruszeniaBezpieczenstwa::~DetektorNaruszeniaBezpieczenstwa() {
    NOTIFY_DESTRUCTOR(~DetektorNaruszeniaBezpieczenstwa, false);
    cleanUpRelations();
}

Sterownik* DetektorNaruszeniaBezpieczenstwa::getItsSterownik() const {
    return itsSterownik;
}

void DetektorNaruszeniaBezpieczenstwa::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool DetektorNaruszeniaBezpieczenstwa::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void DetektorNaruszeniaBezpieczenstwa::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void DetektorNaruszeniaBezpieczenstwa::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void DetektorNaruszeniaBezpieczenstwa::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void DetektorNaruszeniaBezpieczenstwa::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDetektorNaruszeniaBezpieczenstwa::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedDetektorNaruszeniaBezpieczenstwa::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(DetektorNaruszeniaBezpieczenstwa, Default, false, Modul, OMAnimatedModul, OMAnimatedDetektorNaruszeniaBezpieczenstwa)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/DetektorNaruszeniaBezpieczenstwa.cpp
*********************************************************************/
