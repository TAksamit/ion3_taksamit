/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Sterownik.h"
//## link itsWrzeciono
#include "Wrzeciono.h"
//#[ ignore
#define Default_Sterownik_Sterownik_SERIALIZE OM_NO_OP

#define Default_Sterownik_UstawStan_SERIALIZE aomsmethod->addAttribute("stan", x2String((int)stan));

#define Default_Sterownik_message_0_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Sterownik
Sterownik::Sterownik(IOxfActive* theActiveContext) : sygnalizacja(true) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Sterownik, Sterownik(), 0, Default_Sterownik_Sterownik_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsPanel.setShouldDelete(false);
        }
        {
            itsDetektorPoprawnosciDanych.setShouldDelete(false);
        }
        {
            itsDetekcjaKolizji.setShouldDelete(false);
        }
        {
            itsDetektorNaruszeniaBezpieczenstwa.setShouldDelete(false);
        }
        {
            itsEnkoderPozycjiWrzeciona.setShouldDelete(false);
        }
        {
            itsNapedPozycjiWrzeciona_1.setShouldDelete(false);
        }
        {
            itsNapedWrzeciona_1.setShouldDelete(false);
        }
        {
            itsSygnalizacja.setShouldDelete(false);
        }
    }
    itsNapedPozycjiWrzeciona = NULL;
    itsNapedWrzeciona = NULL;
    itsWrzeciono = NULL;
    initRelations();
}

Sterownik::~Sterownik() {
    NOTIFY_DESTRUCTOR(~Sterownik, true);
    cleanUpRelations();
}

void Sterownik::UstawStan(const Stany& stan) {
    NOTIFY_OPERATION(UstawStan, UstawStan(const Stany&), 1, Default_Sterownik_UstawStan_SERIALIZE);
    //#[ operation UstawStan(Stany)
    this->stan = stan;
    //#]
}

void Sterownik::message_0() {
    NOTIFY_OPERATION(message_0, message_0(), 0, Default_Sterownik_message_0_SERIALIZE);
    //#[ operation message_0()
    //#]
}

int Sterownik::getStan() const {
    return stan;
}

void Sterownik::setStan(int p_stan) {
    stan = p_stan;
}

Stany Sterownik::getStan0() const {
    return stan0;
}

void Sterownik::setStan0(Stany p_stan0) {
    stan0 = p_stan0;
}

const bool Sterownik::getSygnalizacja() const {
    return sygnalizacja;
}

DetekcjaKolizji* Sterownik::getItsDetekcjaKolizji() const {
    return (DetekcjaKolizji*) &itsDetekcjaKolizji;
}

DetektorNaruszeniaBezpieczenstwa* Sterownik::getItsDetektorNaruszeniaBezpieczenstwa() const {
    return (DetektorNaruszeniaBezpieczenstwa*) &itsDetektorNaruszeniaBezpieczenstwa;
}

DetektorPoprawnosciDanych* Sterownik::getItsDetektorPoprawnosciDanych() const {
    return (DetektorPoprawnosciDanych*) &itsDetektorPoprawnosciDanych;
}

EnkoderPozycjiWrzeciona* Sterownik::getItsEnkoderPozycjiWrzeciona() const {
    return (EnkoderPozycjiWrzeciona*) &itsEnkoderPozycjiWrzeciona;
}

NapedPozycjiWrzeciona* Sterownik::getItsNapedPozycjiWrzeciona() const {
    return itsNapedPozycjiWrzeciona;
}

void Sterownik::setItsNapedPozycjiWrzeciona(NapedPozycjiWrzeciona* p_NapedPozycjiWrzeciona) {
    _setItsNapedPozycjiWrzeciona(p_NapedPozycjiWrzeciona);
}

NapedPozycjiWrzeciona* Sterownik::getItsNapedPozycjiWrzeciona_1() const {
    return (NapedPozycjiWrzeciona*) &itsNapedPozycjiWrzeciona_1;
}

NapedWrzeciona* Sterownik::getItsNapedWrzeciona() const {
    return itsNapedWrzeciona;
}

void Sterownik::setItsNapedWrzeciona(NapedWrzeciona* p_NapedWrzeciona) {
    _setItsNapedWrzeciona(p_NapedWrzeciona);
}

NapedWrzeciona* Sterownik::getItsNapedWrzeciona_1() const {
    return (NapedWrzeciona*) &itsNapedWrzeciona_1;
}

Panel* Sterownik::getItsPanel() const {
    return (Panel*) &itsPanel;
}

Sygnalizacja* Sterownik::getItsSygnalizacja() const {
    return (Sygnalizacja*) &itsSygnalizacja;
}

Wrzeciono* Sterownik::getItsWrzeciono() const {
    return itsWrzeciono;
}

void Sterownik::setItsWrzeciono(Wrzeciono* p_Wrzeciono) {
    _setItsWrzeciono(p_Wrzeciono);
}

bool Sterownik::startBehavior() {
    bool done = true;
    done &= itsDetekcjaKolizji.startBehavior();
    done &= itsDetektorNaruszeniaBezpieczenstwa.startBehavior();
    done &= itsDetektorPoprawnosciDanych.startBehavior();
    done &= itsEnkoderPozycjiWrzeciona.startBehavior();
    done &= itsNapedPozycjiWrzeciona_1.startBehavior();
    done &= itsNapedWrzeciona_1.startBehavior();
    done &= itsPanel.startBehavior();
    done &= itsSygnalizacja.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void Sterownik::initRelations() {
    itsDetekcjaKolizji._setItsSterownik(this);
    itsDetektorNaruszeniaBezpieczenstwa._setItsSterownik(this);
    itsDetektorPoprawnosciDanych._setItsSterownik(this);
    itsEnkoderPozycjiWrzeciona._setItsSterownik(this);
    itsNapedPozycjiWrzeciona_1._setItsSterownik_1(this);
    itsNapedWrzeciona_1._setItsSterownik_1(this);
    itsPanel._setItsSterownik(this);
    itsSygnalizacja._setItsSterownik(this);
}

void Sterownik::cleanUpRelations() {
    if(itsNapedPozycjiWrzeciona != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsNapedPozycjiWrzeciona");
            itsNapedPozycjiWrzeciona = NULL;
        }
    if(itsNapedWrzeciona != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsNapedWrzeciona");
            itsNapedWrzeciona = NULL;
        }
    if(itsWrzeciono != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsWrzeciono");
            itsWrzeciono = NULL;
        }
}

void Sterownik::__setItsNapedPozycjiWrzeciona(NapedPozycjiWrzeciona* p_NapedPozycjiWrzeciona) {
    itsNapedPozycjiWrzeciona = p_NapedPozycjiWrzeciona;
    if(p_NapedPozycjiWrzeciona != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsNapedPozycjiWrzeciona", p_NapedPozycjiWrzeciona, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsNapedPozycjiWrzeciona");
        }
}

void Sterownik::_setItsNapedPozycjiWrzeciona(NapedPozycjiWrzeciona* p_NapedPozycjiWrzeciona) {
    __setItsNapedPozycjiWrzeciona(p_NapedPozycjiWrzeciona);
}

void Sterownik::_clearItsNapedPozycjiWrzeciona() {
    NOTIFY_RELATION_CLEARED("itsNapedPozycjiWrzeciona");
    itsNapedPozycjiWrzeciona = NULL;
}

void Sterownik::__setItsNapedWrzeciona(NapedWrzeciona* p_NapedWrzeciona) {
    itsNapedWrzeciona = p_NapedWrzeciona;
    if(p_NapedWrzeciona != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsNapedWrzeciona", p_NapedWrzeciona, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsNapedWrzeciona");
        }
}

void Sterownik::_setItsNapedWrzeciona(NapedWrzeciona* p_NapedWrzeciona) {
    __setItsNapedWrzeciona(p_NapedWrzeciona);
}

void Sterownik::_clearItsNapedWrzeciona() {
    NOTIFY_RELATION_CLEARED("itsNapedWrzeciona");
    itsNapedWrzeciona = NULL;
}

void Sterownik::__setItsWrzeciono(Wrzeciono* p_Wrzeciono) {
    itsWrzeciono = p_Wrzeciono;
    if(p_Wrzeciono != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsWrzeciono", p_Wrzeciono, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsWrzeciono");
        }
}

void Sterownik::_setItsWrzeciono(Wrzeciono* p_Wrzeciono) {
    __setItsWrzeciono(p_Wrzeciono);
}

void Sterownik::_clearItsWrzeciono() {
    NOTIFY_RELATION_CLEARED("itsWrzeciono");
    itsWrzeciono = NULL;
}

void Sterownik::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsPanel.setActiveContext(theActiveContext, false);
        itsDetektorPoprawnosciDanych.setActiveContext(theActiveContext, false);
        itsDetekcjaKolizji.setActiveContext(theActiveContext, false);
        itsDetektorNaruszeniaBezpieczenstwa.setActiveContext(theActiveContext, false);
        itsEnkoderPozycjiWrzeciona.setActiveContext(theActiveContext, false);
        itsNapedPozycjiWrzeciona_1.setActiveContext(theActiveContext, false);
        itsNapedWrzeciona_1.setActiveContext(theActiveContext, false);
        itsSygnalizacja.setActiveContext(theActiveContext, false);
    }
}

void Sterownik::destroy() {
    itsDetekcjaKolizji.destroy();
    itsDetektorNaruszeniaBezpieczenstwa.destroy();
    itsDetektorPoprawnosciDanych.destroy();
    itsEnkoderPozycjiWrzeciona.destroy();
    itsNapedPozycjiWrzeciona_1.destroy();
    itsNapedWrzeciona_1.destroy();
    itsPanel.destroy();
    itsSygnalizacja.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSterownik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("stan", x2String(myReal->stan));
    aomsAttributes->addAttribute("stan0", x2String((int)myReal->stan0));
    aomsAttributes->addAttribute("sygnalizacja", x2String(myReal->sygnalizacja));
}

void OMAnimatedSterownik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsNapedPozycjiWrzeciona", false, true);
    if(myReal->itsNapedPozycjiWrzeciona)
        {
            aomsRelations->ADD_ITEM(myReal->itsNapedPozycjiWrzeciona);
        }
    aomsRelations->addRelation("itsNapedWrzeciona", false, true);
    if(myReal->itsNapedWrzeciona)
        {
            aomsRelations->ADD_ITEM(myReal->itsNapedWrzeciona);
        }
    aomsRelations->addRelation("itsPanel", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsPanel);
    aomsRelations->addRelation("itsDetektorPoprawnosciDanych", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDetektorPoprawnosciDanych);
    aomsRelations->addRelation("itsDetekcjaKolizji", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDetekcjaKolizji);
    aomsRelations->addRelation("itsDetektorNaruszeniaBezpieczenstwa", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDetektorNaruszeniaBezpieczenstwa);
    aomsRelations->addRelation("itsEnkoderPozycjiWrzeciona", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsEnkoderPozycjiWrzeciona);
    aomsRelations->addRelation("itsNapedPozycjiWrzeciona_1", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsNapedPozycjiWrzeciona_1);
    aomsRelations->addRelation("itsNapedWrzeciona_1", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsNapedWrzeciona_1);
    aomsRelations->addRelation("itsSygnalizacja", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSygnalizacja);
    aomsRelations->addRelation("itsWrzeciono", false, true);
    if(myReal->itsWrzeciono)
        {
            aomsRelations->ADD_ITEM(myReal->itsWrzeciono);
        }
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(Sterownik, Default, Default, false, OMAnimatedSterownik)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.cpp
*********************************************************************/
