/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: EnkoderPozycjiWrzeciona
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/EnkoderPozycjiWrzeciona.h
*********************************************************************/

#ifndef EnkoderPozycjiWrzeciona_H
#define EnkoderPozycjiWrzeciona_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class EnkoderPozycjiWrzeciona
#include "Modul.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class EnkoderPozycjiWrzeciona
class EnkoderPozycjiWrzeciona : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedEnkoderPozycjiWrzeciona;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    EnkoderPozycjiWrzeciona(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~EnkoderPozycjiWrzeciona();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getOkres() const;
    
    //## auto_generated
    void setOkres(int p_okres);
    
    //## auto_generated
    float getPredkosc() const;
    
    //## auto_generated
    void setPredkosc(float p_predkosc);
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    int okres;		//## attribute okres
    
    float predkosc;		//## attribute predkosc
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedEnkoderPozycjiWrzeciona : public OMAnimatedModul {
    DECLARE_META(EnkoderPozycjiWrzeciona, OMAnimatedEnkoderPozycjiWrzeciona)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/EnkoderPozycjiWrzeciona.h
*********************************************************************/
