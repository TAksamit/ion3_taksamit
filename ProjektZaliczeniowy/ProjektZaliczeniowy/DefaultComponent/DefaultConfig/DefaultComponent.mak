
############# Target type (Debug/Release) ##################
############################################################
CPPCompileDebug=-g
CPPCompileRelease=-O
LinkDebug=-g
LinkRelease=-O

CleanupFlagForSimulink=
SIMULINK_CONFIG=False
ifeq ($(SIMULINK_CONFIG),True)
CleanupFlagForSimulink=-DOM_WITH_CLEANUP
endif

ConfigurationCPPCompileSwitches=   $(INCLUDE_QUALIFIER). $(INCLUDE_QUALIFIER)$(OMROOT) $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/oxf $(DEFINE_QUALIFIER)CYGWIN $(INST_FLAGS) $(INCLUDE_PATH) $(INST_INCLUDES) -Wno-write-strings $(CPPCompileDebug) -c  $(CleanupFlagForSimulink)
ConfigurationCCCompileSwitches=$(INCLUDE_PATH) -c 

#########################################
###### Predefined macros ################
RM=/bin/rm -rf
INCLUDE_QUALIFIER=-I
DEFINE_QUALIFIER=-D
CC=g++
LIB_CMD=ar
LINK_CMD=g++
LIB_FLAGS=rvu
LINK_FLAGS= $(LinkDebug)   

#########################################
####### Context macros ##################

FLAGSFILE=
RULESFILE=
OMROOT="C:/Program Files/IBM/Rhapsody/9.0.1/Share"
RHPROOT="C:/Program Files/IBM/Rhapsody/9.0.1"
FRAMEWORK_LIB_ROOT="C:/ProgramData/IBM/Rhapsody/9.0.1x64/UserShare"

CPP_EXT=.cpp
H_EXT=.h
OBJ_EXT=.o
EXE_EXT=.exe
LIB_EXT=.a

INSTRUMENTATION=Animation

TIME_MODEL=RealTime

TARGET_TYPE=Executable

TARGET_NAME=DefaultComponent

all : $(TARGET_NAME)$(EXE_EXT) DefaultComponent.mak

TARGET_MAIN=MainDefaultComponent

LIBS=

INCLUDE_PATH= \
  $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/osconfig/Cygwin

ADDITIONAL_OBJS=

OBJS= \
  Panel.o \
  Sterownik.o \
  NapedWrzeciona.o \
  EnkoderPozycjiWrzeciona.o \
  NapedPozycjiWrzeciona.o \
  DetekcjaKolizji.o \
  DetektorNaruszeniaBezpieczenstwa.o \
  DetektorPoprawnosciDanych.o \
  Sygnalizacja.o \
  Wrzeciono.o \
  Modul.o \
  Ustawienia.o \
  Operator.o \
  Uzytkownik.o \
  Administrator.o \
  UtrzymanieRuchu.o \
  Otoczenie.o \
  Default.o




#########################################
####### Predefined macros ###############
$(OBJS) : $(INST_LIBS) $(OXF_LIBS)

ifeq ($(INSTRUMENTATION),Animation)

INST_FLAGS=$(DEFINE_QUALIFIER)OMANIMATOR $(DEFINE_QUALIFIER)__USE_W32_SOCKETS 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwinaomanimx64$(LIB_EXT) $(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwinoxsiminstx64$(LIB_EXT)
OXF_LIBS=$(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwinoxfinstx64$(LIB_EXT) $(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwinomcomapplx64$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),Tracing)

INST_FLAGS=$(DEFINE_QUALIFIER)OMTRACER 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwintomtracex64$(LIB_EXT) $(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwinaomtracex64$(LIB_EXT) $(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwinoxsiminstx64$(LIB_EXT)
OXF_LIBS=$(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwinoxfinstx64$(LIB_EXT) $(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwinomcomapplx64$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),None)

INST_FLAGS= 
INST_INCLUDES=
INST_LIBS=$(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwinoxsimx64$(LIB_EXT)
OXF_LIBS=$(FRAMEWORK_LIB_ROOT)/LangCpp/lib/cygwinoxfx64$(LIB_EXT)
SOCK_LIB=-lws2_32

else
	@echo An invalid Instrumentation $(INSTRUMENTATION) is specified.
	exit
endif
endif
endif

.SUFFIXES: $(CPP_EXT)

#####################################################################
##################### Context dependencies and commands #############






Panel.o : Panel.cpp Panel.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Panel.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Panel.o Panel.cpp




Sterownik.o : Sterownik.cpp Sterownik.h    Default.h NapedPozycjiWrzeciona.h NapedWrzeciona.h Panel.h DetektorPoprawnosciDanych.h DetekcjaKolizji.h DetektorNaruszeniaBezpieczenstwa.h EnkoderPozycjiWrzeciona.h Sygnalizacja.h Wrzeciono.h Modul.h 
	@echo Compiling Sterownik.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Sterownik.o Sterownik.cpp




NapedWrzeciona.o : NapedWrzeciona.cpp NapedWrzeciona.h    Default.h Sterownik.h Modul.h 
	@echo Compiling NapedWrzeciona.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o NapedWrzeciona.o NapedWrzeciona.cpp




EnkoderPozycjiWrzeciona.o : EnkoderPozycjiWrzeciona.cpp EnkoderPozycjiWrzeciona.h    Default.h Sterownik.h Modul.h 
	@echo Compiling EnkoderPozycjiWrzeciona.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o EnkoderPozycjiWrzeciona.o EnkoderPozycjiWrzeciona.cpp




NapedPozycjiWrzeciona.o : NapedPozycjiWrzeciona.cpp NapedPozycjiWrzeciona.h    Default.h Sterownik.h Modul.h 
	@echo Compiling NapedPozycjiWrzeciona.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o NapedPozycjiWrzeciona.o NapedPozycjiWrzeciona.cpp




DetekcjaKolizji.o : DetekcjaKolizji.cpp DetekcjaKolizji.h    Default.h Sterownik.h Modul.h 
	@echo Compiling DetekcjaKolizji.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o DetekcjaKolizji.o DetekcjaKolizji.cpp




DetektorNaruszeniaBezpieczenstwa.o : DetektorNaruszeniaBezpieczenstwa.cpp DetektorNaruszeniaBezpieczenstwa.h    Default.h Sterownik.h Modul.h 
	@echo Compiling DetektorNaruszeniaBezpieczenstwa.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o DetektorNaruszeniaBezpieczenstwa.o DetektorNaruszeniaBezpieczenstwa.cpp




DetektorPoprawnosciDanych.o : DetektorPoprawnosciDanych.cpp DetektorPoprawnosciDanych.h    Default.h Sterownik.h Modul.h 
	@echo Compiling DetektorPoprawnosciDanych.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o DetektorPoprawnosciDanych.o DetektorPoprawnosciDanych.cpp




Sygnalizacja.o : Sygnalizacja.cpp Sygnalizacja.h    Default.h Sterownik.h Modul.h 
	@echo Compiling Sygnalizacja.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Sygnalizacja.o Sygnalizacja.cpp




Wrzeciono.o : Wrzeciono.cpp Wrzeciono.h    Default.h Sterownik.h Ustawienia.h Modul.h 
	@echo Compiling Wrzeciono.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Wrzeciono.o Wrzeciono.cpp




Modul.o : Modul.cpp Modul.h    Default.h 
	@echo Compiling Modul.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Modul.o Modul.cpp




Ustawienia.o : Ustawienia.cpp Ustawienia.h    Default.h Wrzeciono.h 
	@echo Compiling Ustawienia.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Ustawienia.o Ustawienia.cpp




Operator.o : Operator.cpp Operator.h    Default.h 
	@echo Compiling Operator.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Operator.o Operator.cpp




Uzytkownik.o : Uzytkownik.cpp Uzytkownik.h    Default.h Operator.h 
	@echo Compiling Uzytkownik.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Uzytkownik.o Uzytkownik.cpp




Administrator.o : Administrator.cpp Administrator.h    Default.h Operator.h 
	@echo Compiling Administrator.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Administrator.o Administrator.cpp




UtrzymanieRuchu.o : UtrzymanieRuchu.cpp UtrzymanieRuchu.h    Default.h Operator.h 
	@echo Compiling UtrzymanieRuchu.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o UtrzymanieRuchu.o UtrzymanieRuchu.cpp




Otoczenie.o : Otoczenie.cpp Otoczenie.h    Default.h 
	@echo Compiling Otoczenie.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Otoczenie.o Otoczenie.cpp




Default.o : Default.cpp Default.h    Panel.h Sterownik.h NapedWrzeciona.h EnkoderPozycjiWrzeciona.h NapedPozycjiWrzeciona.h DetekcjaKolizji.h DetektorNaruszeniaBezpieczenstwa.h DetektorPoprawnosciDanych.h Sygnalizacja.h Wrzeciono.h Modul.h Ustawienia.h 
	@echo Compiling Default.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Default.o Default.cpp







$(TARGET_MAIN)$(OBJ_EXT) : $(TARGET_MAIN)$(CPP_EXT) $(OBJS)
	@echo Compiling $(TARGET_MAIN)$(CPP_EXT)
	@$(CC) $(ConfigurationCPPCompileSwitches) -o  $(TARGET_MAIN)$(OBJ_EXT) $(TARGET_MAIN)$(CPP_EXT)

####################################################################
############## Predefined Instructions #############################
$(TARGET_NAME)$(EXE_EXT): $(OBJS) $(ADDITIONAL_OBJS) $(TARGET_MAIN)$(OBJ_EXT) DefaultComponent.mak
	@echo Linking $(TARGET_NAME)$(EXE_EXT)
	@$(LINK_CMD)  $(TARGET_MAIN)$(OBJ_EXT) $(OBJS) $(ADDITIONAL_OBJS) \
	$(LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(SOCK_LIB) \
	$(LINK_FLAGS) -o $(TARGET_NAME)$(EXE_EXT)

$(TARGET_NAME)$(LIB_EXT) : $(OBJS) $(ADDITIONAL_OBJS) DefaultComponent.mak
	@echo Building library $@
	@$(LIB_CMD) $(LIB_FLAGS) $(TARGET_NAME)$(LIB_EXT) $(OBJS) $(ADDITIONAL_OBJS)



clean:
	@echo Cleanup
	$(RM) Panel.o
	$(RM) Sterownik.o
	$(RM) NapedWrzeciona.o
	$(RM) EnkoderPozycjiWrzeciona.o
	$(RM) NapedPozycjiWrzeciona.o
	$(RM) DetekcjaKolizji.o
	$(RM) DetektorNaruszeniaBezpieczenstwa.o
	$(RM) DetektorPoprawnosciDanych.o
	$(RM) Sygnalizacja.o
	$(RM) Wrzeciono.o
	$(RM) Modul.o
	$(RM) Ustawienia.o
	$(RM) Operator.o
	$(RM) Uzytkownik.o
	$(RM) Administrator.o
	$(RM) UtrzymanieRuchu.o
	$(RM) Otoczenie.o
	$(RM) Default.o
	$(RM) $(TARGET_MAIN)$(OBJ_EXT) $(ADDITIONAL_OBJS)
	$(RM) $(TARGET_NAME)$(LIB_EXT)
	$(RM) $(TARGET_NAME)$(EXE_EXT)

