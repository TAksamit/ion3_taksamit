/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Wrzeciono
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Wrzeciono.h
*********************************************************************/

#ifndef Wrzeciono_H
#define Wrzeciono_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsSterownik
#include "Sterownik.h"
//## link itsUstawienia
#include "Ustawienia.h"
//## package Default

//## class Wrzeciono
class Wrzeciono : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedWrzeciono;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Wrzeciono(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Wrzeciono();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    Ustawienia* getItsUstawienia() const;
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    ////    Relations and components    ////
    
    Sterownik itsSterownik;		//## link itsSterownik
    
    Ustawienia itsUstawienia;		//## link itsUstawienia
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedWrzeciono : virtual public AOMInstance {
    DECLARE_META(Wrzeciono, OMAnimatedWrzeciono)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Wrzeciono.h
*********************************************************************/
