/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DetektorPoprawnosciDanych
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/DetektorPoprawnosciDanych.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "DetektorPoprawnosciDanych.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_DetektorPoprawnosciDanych_DetektorPoprawnosciDanych_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class DetektorPoprawnosciDanych
DetektorPoprawnosciDanych::DetektorPoprawnosciDanych(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(DetektorPoprawnosciDanych, DetektorPoprawnosciDanych(), 0, Default_DetektorPoprawnosciDanych_DetektorPoprawnosciDanych_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
}

DetektorPoprawnosciDanych::~DetektorPoprawnosciDanych() {
    NOTIFY_DESTRUCTOR(~DetektorPoprawnosciDanych, false);
    cleanUpRelations();
}

Sterownik* DetektorPoprawnosciDanych::getItsSterownik() const {
    return itsSterownik;
}

void DetektorPoprawnosciDanych::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool DetektorPoprawnosciDanych::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void DetektorPoprawnosciDanych::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void DetektorPoprawnosciDanych::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void DetektorPoprawnosciDanych::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void DetektorPoprawnosciDanych::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDetektorPoprawnosciDanych::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedDetektorPoprawnosciDanych::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(DetektorPoprawnosciDanych, Default, false, Modul, OMAnimatedModul, OMAnimatedDetektorPoprawnosciDanych)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/DetektorPoprawnosciDanych.cpp
*********************************************************************/
