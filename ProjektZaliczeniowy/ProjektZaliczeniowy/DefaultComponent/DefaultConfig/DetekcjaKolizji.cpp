/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DetekcjaKolizji
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/DetekcjaKolizji.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "DetekcjaKolizji.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_DetekcjaKolizji_DetekcjaKolizji_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class DetekcjaKolizji
DetekcjaKolizji::DetekcjaKolizji(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(DetekcjaKolizji, DetekcjaKolizji(), 0, Default_DetekcjaKolizji_DetekcjaKolizji_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
}

DetekcjaKolizji::~DetekcjaKolizji() {
    NOTIFY_DESTRUCTOR(~DetekcjaKolizji, false);
    cleanUpRelations();
}

Sterownik* DetekcjaKolizji::getItsSterownik() const {
    return itsSterownik;
}

void DetekcjaKolizji::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool DetekcjaKolizji::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void DetekcjaKolizji::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void DetekcjaKolizji::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void DetekcjaKolizji::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void DetekcjaKolizji::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDetekcjaKolizji::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedDetekcjaKolizji::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(DetekcjaKolizji, Default, false, Modul, OMAnimatedModul, OMAnimatedDetekcjaKolizji)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/DetekcjaKolizji.cpp
*********************************************************************/
