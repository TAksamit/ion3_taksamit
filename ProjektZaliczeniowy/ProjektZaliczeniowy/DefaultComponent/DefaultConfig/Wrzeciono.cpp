/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Wrzeciono
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Wrzeciono.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Wrzeciono.h"
//#[ ignore
#define Default_Wrzeciono_Wrzeciono_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Wrzeciono
Wrzeciono::Wrzeciono(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Wrzeciono, Wrzeciono(), 0, Default_Wrzeciono_Wrzeciono_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsSterownik.setShouldDelete(false);
        }
    }
    initRelations();
}

Wrzeciono::~Wrzeciono() {
    NOTIFY_DESTRUCTOR(~Wrzeciono, true);
}

Sterownik* Wrzeciono::getItsSterownik() const {
    return (Sterownik*) &itsSterownik;
}

Ustawienia* Wrzeciono::getItsUstawienia() const {
    return (Ustawienia*) &itsUstawienia;
}

bool Wrzeciono::startBehavior() {
    bool done = true;
    done &= itsSterownik.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void Wrzeciono::initRelations() {
    itsSterownik._setItsWrzeciono(this);
    itsUstawienia._setItsWrzeciono(this);
}

void Wrzeciono::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsSterownik.setActiveContext(theActiveContext, false);
    }
}

void Wrzeciono::destroy() {
    itsSterownik.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedWrzeciono::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSterownik);
    aomsRelations->addRelation("itsUstawienia", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsUstawienia);
}
//#]

IMPLEMENT_REACTIVE_META_SIMPLE_P(Wrzeciono, Default, Default, false, OMAnimatedWrzeciono)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Wrzeciono.cpp
*********************************************************************/
