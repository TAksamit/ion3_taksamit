/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sygnalizacja
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Sygnalizacja.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Sygnalizacja.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Sygnalizacja_Sygnalizacja_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Sygnalizacja
Sygnalizacja::Sygnalizacja(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Sygnalizacja, Sygnalizacja(), 0, Default_Sygnalizacja_Sygnalizacja_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
}

Sygnalizacja::~Sygnalizacja() {
    NOTIFY_DESTRUCTOR(~Sygnalizacja, false);
    cleanUpRelations();
}

Sterownik* Sygnalizacja::getItsSterownik() const {
    return itsSterownik;
}

void Sygnalizacja::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Sygnalizacja::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Sygnalizacja::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void Sygnalizacja::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Sygnalizacja::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Sygnalizacja::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSygnalizacja::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedSygnalizacja::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(Sygnalizacja, Default, false, Modul, OMAnimatedModul, OMAnimatedSygnalizacja)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sygnalizacja.cpp
*********************************************************************/
