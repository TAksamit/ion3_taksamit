/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: NapedPozycjiWrzeciona
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/NapedPozycjiWrzeciona.h
*********************************************************************/

#ifndef NapedPozycjiWrzeciona_H
#define NapedPozycjiWrzeciona_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class NapedPozycjiWrzeciona
#include "Modul.h"
//## link itsSterownik
#include "Sterownik.h"
//## package Default

//## class NapedPozycjiWrzeciona
class NapedPozycjiWrzeciona : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedNapedPozycjiWrzeciona;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    NapedPozycjiWrzeciona(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~NapedPozycjiWrzeciona();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    Sterownik* getItsSterownik_1() const;
    
    //## auto_generated
    void setItsSterownik_1(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Sterownik itsSterownik;		//## link itsSterownik
    
    Sterownik* itsSterownik_1;		//## link itsSterownik_1
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik_1(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik_1(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik_1();
    
    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedNapedPozycjiWrzeciona : public OMAnimatedModul {
    DECLARE_META(NapedPozycjiWrzeciona, OMAnimatedNapedPozycjiWrzeciona)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/NapedPozycjiWrzeciona.h
*********************************************************************/
