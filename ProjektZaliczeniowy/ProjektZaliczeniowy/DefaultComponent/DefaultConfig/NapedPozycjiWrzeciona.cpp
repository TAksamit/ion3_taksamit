/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: NapedPozycjiWrzeciona
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/NapedPozycjiWrzeciona.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "NapedPozycjiWrzeciona.h"
//#[ ignore
#define Default_NapedPozycjiWrzeciona_NapedPozycjiWrzeciona_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class NapedPozycjiWrzeciona
NapedPozycjiWrzeciona::NapedPozycjiWrzeciona(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(NapedPozycjiWrzeciona, NapedPozycjiWrzeciona(), 0, Default_NapedPozycjiWrzeciona_NapedPozycjiWrzeciona_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsSterownik.setShouldDelete(false);
        }
    }
    itsSterownik_1 = NULL;
    initRelations();
}

NapedPozycjiWrzeciona::~NapedPozycjiWrzeciona() {
    NOTIFY_DESTRUCTOR(~NapedPozycjiWrzeciona, false);
    cleanUpRelations();
}

Sterownik* NapedPozycjiWrzeciona::getItsSterownik() const {
    return (Sterownik*) &itsSterownik;
}

Sterownik* NapedPozycjiWrzeciona::getItsSterownik_1() const {
    return itsSterownik_1;
}

void NapedPozycjiWrzeciona::setItsSterownik_1(Sterownik* p_Sterownik) {
    _setItsSterownik_1(p_Sterownik);
}

bool NapedPozycjiWrzeciona::startBehavior() {
    bool done = true;
    done &= itsSterownik.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void NapedPozycjiWrzeciona::initRelations() {
    itsSterownik._setItsNapedPozycjiWrzeciona(this);
}

void NapedPozycjiWrzeciona::cleanUpRelations() {
    if(itsSterownik_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_1");
            itsSterownik_1 = NULL;
        }
}

void NapedPozycjiWrzeciona::__setItsSterownik_1(Sterownik* p_Sterownik) {
    itsSterownik_1 = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik_1", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_1");
        }
}

void NapedPozycjiWrzeciona::_setItsSterownik_1(Sterownik* p_Sterownik) {
    __setItsSterownik_1(p_Sterownik);
}

void NapedPozycjiWrzeciona::_clearItsSterownik_1() {
    NOTIFY_RELATION_CLEARED("itsSterownik_1");
    itsSterownik_1 = NULL;
}

void NapedPozycjiWrzeciona::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsSterownik.setActiveContext(theActiveContext, false);
    }
}

void NapedPozycjiWrzeciona::destroy() {
    itsSterownik.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedNapedPozycjiWrzeciona::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedNapedPozycjiWrzeciona::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSterownik);
    aomsRelations->addRelation("itsSterownik_1", false, true);
    if(myReal->itsSterownik_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik_1);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(NapedPozycjiWrzeciona, Default, false, Modul, OMAnimatedModul, OMAnimatedNapedPozycjiWrzeciona)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/NapedPozycjiWrzeciona.cpp
*********************************************************************/
