/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Administrator
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Administrator.h
*********************************************************************/

#ifndef Administrator_H
#define Administrator_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## actor Administrator
#include "Operator.h"
//## package Default

//## actor Administrator
class Administrator : public Operator {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedAdministrator;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Administrator();
    
    //## auto_generated
    ~Administrator();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedAdministrator : public OMAnimatedOperator {
    DECLARE_META(Administrator, OMAnimatedAdministrator)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Administrator.h
*********************************************************************/
