/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Modul
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Modul.h
*********************************************************************/

#ifndef Modul_H
#define Modul_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## package Default

//## class Modul
class Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedModul;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Modul();
    
    //## auto_generated
    virtual ~Modul() = 0;
    
    ////    Operations    ////
    
    //## operation ZapiszUstawienia(std::string)
    virtual bool ZapiszUstawienia(std::string nastawy) = 0;
    
    //## operation czytajUstawienia()
    virtual std::string czytajUstawienia() = 0;
    
    ////    Additional operations    ////

protected :

    //## auto_generated
    int getId() const;
    
    //## auto_generated
    void setId(int p_id);
    
    //## auto_generated
    std::string getNazwa() const;
    
    //## auto_generated
    void setNazwa(std::string p_nazwa);
    
    ////    Attributes    ////
    
    int id;		//## attribute id
    
    std::string nazwa;		//## attribute nazwa
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedModul : virtual public AOMInstance {
    DECLARE_META(Modul, OMAnimatedModul)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Modul.h
*********************************************************************/
