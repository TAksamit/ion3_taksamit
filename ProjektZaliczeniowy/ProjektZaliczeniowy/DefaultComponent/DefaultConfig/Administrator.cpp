/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Administrator
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Administrator.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Administrator.h"
//#[ ignore
#define Default_Administrator_Administrator_SERIALIZE OM_NO_OP
//#]

//## package Default

//## actor Administrator
Administrator::Administrator() {
    NOTIFY_CONSTRUCTOR(Administrator, Administrator(), 0, Default_Administrator_Administrator_SERIALIZE);
}

Administrator::~Administrator() {
    NOTIFY_DESTRUCTOR(~Administrator, false);
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedAdministrator::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedOperator::serializeAttributes(aomsAttributes);
}

void OMAnimatedAdministrator::serializeRelations(AOMSRelations* aomsRelations) const {
    OMAnimatedOperator::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_META_S_P(Administrator, Default, false, Operator, OMAnimatedOperator, OMAnimatedAdministrator)

OMINIT_SUPERCLASS(Operator, OMAnimatedOperator)

OMREGISTER_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Administrator.cpp
*********************************************************************/
