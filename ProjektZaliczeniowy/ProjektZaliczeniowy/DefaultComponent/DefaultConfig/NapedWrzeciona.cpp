/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: NapedWrzeciona
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/NapedWrzeciona.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "NapedWrzeciona.h"
//#[ ignore
#define Default_NapedWrzeciona_NapedWrzeciona_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class NapedWrzeciona
NapedWrzeciona::NapedWrzeciona(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(NapedWrzeciona, NapedWrzeciona(), 0, Default_NapedWrzeciona_NapedWrzeciona_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsSterownik.setShouldDelete(false);
        }
    }
    itsSterownik_1 = NULL;
    initRelations();
}

NapedWrzeciona::~NapedWrzeciona() {
    NOTIFY_DESTRUCTOR(~NapedWrzeciona, false);
    cleanUpRelations();
}

Sterownik* NapedWrzeciona::getItsSterownik() const {
    return (Sterownik*) &itsSterownik;
}

Sterownik* NapedWrzeciona::getItsSterownik_1() const {
    return itsSterownik_1;
}

void NapedWrzeciona::setItsSterownik_1(Sterownik* p_Sterownik) {
    _setItsSterownik_1(p_Sterownik);
}

bool NapedWrzeciona::startBehavior() {
    bool done = true;
    done &= itsSterownik.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void NapedWrzeciona::initRelations() {
    itsSterownik._setItsNapedWrzeciona(this);
}

void NapedWrzeciona::cleanUpRelations() {
    if(itsSterownik_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_1");
            itsSterownik_1 = NULL;
        }
}

void NapedWrzeciona::__setItsSterownik_1(Sterownik* p_Sterownik) {
    itsSterownik_1 = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik_1", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik_1");
        }
}

void NapedWrzeciona::_setItsSterownik_1(Sterownik* p_Sterownik) {
    __setItsSterownik_1(p_Sterownik);
}

void NapedWrzeciona::_clearItsSterownik_1() {
    NOTIFY_RELATION_CLEARED("itsSterownik_1");
    itsSterownik_1 = NULL;
}

void NapedWrzeciona::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsSterownik.setActiveContext(theActiveContext, false);
    }
}

void NapedWrzeciona::destroy() {
    itsSterownik.destroy();
    OMReactive::destroy();
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedNapedWrzeciona::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedNapedWrzeciona::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSterownik);
    aomsRelations->addRelation("itsSterownik_1", false, true);
    if(myReal->itsSterownik_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik_1);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(NapedWrzeciona, Default, false, Modul, OMAnimatedModul, OMAnimatedNapedWrzeciona)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/NapedWrzeciona.cpp
*********************************************************************/
