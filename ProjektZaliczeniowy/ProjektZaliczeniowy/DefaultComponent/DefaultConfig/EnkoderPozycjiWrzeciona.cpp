/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: EnkoderPozycjiWrzeciona
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/EnkoderPozycjiWrzeciona.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "EnkoderPozycjiWrzeciona.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_EnkoderPozycjiWrzeciona_EnkoderPozycjiWrzeciona_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class EnkoderPozycjiWrzeciona
EnkoderPozycjiWrzeciona::EnkoderPozycjiWrzeciona(IOxfActive* theActiveContext) : okres(250), predkosc(1.) {
    NOTIFY_REACTIVE_CONSTRUCTOR(EnkoderPozycjiWrzeciona, EnkoderPozycjiWrzeciona(), 0, Default_EnkoderPozycjiWrzeciona_EnkoderPozycjiWrzeciona_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
}

EnkoderPozycjiWrzeciona::~EnkoderPozycjiWrzeciona() {
    NOTIFY_DESTRUCTOR(~EnkoderPozycjiWrzeciona, false);
    cleanUpRelations();
}

int EnkoderPozycjiWrzeciona::getOkres() const {
    return okres;
}

void EnkoderPozycjiWrzeciona::setOkres(int p_okres) {
    okres = p_okres;
}

float EnkoderPozycjiWrzeciona::getPredkosc() const {
    return predkosc;
}

void EnkoderPozycjiWrzeciona::setPredkosc(float p_predkosc) {
    predkosc = p_predkosc;
}

Sterownik* EnkoderPozycjiWrzeciona::getItsSterownik() const {
    return itsSterownik;
}

void EnkoderPozycjiWrzeciona::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool EnkoderPozycjiWrzeciona::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void EnkoderPozycjiWrzeciona::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void EnkoderPozycjiWrzeciona::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void EnkoderPozycjiWrzeciona::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void EnkoderPozycjiWrzeciona::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedEnkoderPozycjiWrzeciona::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("okres", x2String(myReal->okres));
    aomsAttributes->addAttribute("predkosc", x2String(myReal->predkosc));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedEnkoderPozycjiWrzeciona::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(EnkoderPozycjiWrzeciona, Default, false, Modul, OMAnimatedModul, OMAnimatedEnkoderPozycjiWrzeciona)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/EnkoderPozycjiWrzeciona.cpp
*********************************************************************/
