/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ustawienia
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.h
*********************************************************************/

#ifndef Ustawienia_H
#define Ustawienia_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## link itsWrzeciono
class Wrzeciono;

//## package Default

//## class Ustawienia
class Ustawienia {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedUstawienia;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Ustawienia();
    
    //## auto_generated
    ~Ustawienia();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Wrzeciono* getItsWrzeciono() const;
    
    //## auto_generated
    void setItsWrzeciono(Wrzeciono* p_Wrzeciono);

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Wrzeciono* itsWrzeciono;		//## link itsWrzeciono
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsWrzeciono(Wrzeciono* p_Wrzeciono);
    
    //## auto_generated
    void _setItsWrzeciono(Wrzeciono* p_Wrzeciono);
    
    //## auto_generated
    void _clearItsWrzeciono();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedUstawienia : virtual public AOMInstance {
    DECLARE_META(Ustawienia, OMAnimatedUstawienia)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.h
*********************************************************************/
