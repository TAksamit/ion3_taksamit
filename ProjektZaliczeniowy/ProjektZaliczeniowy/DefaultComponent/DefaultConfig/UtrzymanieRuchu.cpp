/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: UtrzymanieRuchu
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/UtrzymanieRuchu.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "UtrzymanieRuchu.h"
//#[ ignore
#define Default_UtrzymanieRuchu_UtrzymanieRuchu_SERIALIZE OM_NO_OP
//#]

//## package Default

//## actor UtrzymanieRuchu
UtrzymanieRuchu::UtrzymanieRuchu() {
    NOTIFY_CONSTRUCTOR(UtrzymanieRuchu, UtrzymanieRuchu(), 0, Default_UtrzymanieRuchu_UtrzymanieRuchu_SERIALIZE);
}

UtrzymanieRuchu::~UtrzymanieRuchu() {
    NOTIFY_DESTRUCTOR(~UtrzymanieRuchu, false);
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedUtrzymanieRuchu::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedOperator::serializeAttributes(aomsAttributes);
}

void OMAnimatedUtrzymanieRuchu::serializeRelations(AOMSRelations* aomsRelations) const {
    OMAnimatedOperator::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_META_S_P(UtrzymanieRuchu, Default, false, Operator, OMAnimatedOperator, OMAnimatedUtrzymanieRuchu)

OMINIT_SUPERCLASS(Operator, OMAnimatedOperator)

OMREGISTER_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/UtrzymanieRuchu.cpp
*********************************************************************/
