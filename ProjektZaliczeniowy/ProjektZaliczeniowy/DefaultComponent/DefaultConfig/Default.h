/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/

#ifndef Default_H
#define Default_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
class DetekcjaKolizji;

//## auto_generated
class DetektorNaruszeniaBezpieczenstwa;

//## auto_generated
class DetektorPoprawnosciDanych;

//## auto_generated
class EnkoderPozycjiWrzeciona;

//## auto_generated
class Modul;

//## auto_generated
class NapedPozycjiWrzeciona;

//## auto_generated
class NapedWrzeciona;

//## auto_generated
class Panel;

//## auto_generated
class Sterownik;

//## auto_generated
class Sygnalizacja;

//## auto_generated
class Ustawienia;

//## auto_generated
class Wrzeciono;

//#[ ignore
#define evImpuls_Default_id 18601

#define evValidate_Default_id 18602

#define evDekoduj_Default_id 18603

#define evStop_Default_id 18604

#define evStart_Default_id 18605

#define evKrokRuchu_Default_id 18606

#define evSprawdzKolizje_Default_id 18607

#define evKolizja_Default_id 18608

#define eventmessage_0_Default_id 18609

#define evCzerwone_Default_id 18610

#define evSwiec_Default_id 18611

#define evAwaria_Default_id 18612

#define evNaruszenie_Default_id 18613
//#]

//## package Default


//## type Stany
enum Stany {
    WylaczanieMaszyny = 1,
    ZatrzymanieMaszyny = 0,
    UruchamianieMaszyny = -1
};

//## event evImpuls()
class evImpuls : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevImpuls;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evImpuls();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevImpuls : virtual public AOMEvent {
    DECLARE_META_EVENT(evImpuls)
};
//#]
#endif // _OMINSTRUMENT

//## event evValidate()
class evValidate : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevValidate;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evValidate();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevValidate : virtual public AOMEvent {
    DECLARE_META_EVENT(evValidate)
};
//#]
#endif // _OMINSTRUMENT

//## event evDekoduj()
class evDekoduj : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevDekoduj;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evDekoduj();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevDekoduj : virtual public AOMEvent {
    DECLARE_META_EVENT(evDekoduj)
};
//#]
#endif // _OMINSTRUMENT

//## event evStop()
class evStop : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStop;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStop();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStop : virtual public AOMEvent {
    DECLARE_META_EVENT(evStop)
};
//#]
#endif // _OMINSTRUMENT

//## event evStart()
class evStart : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStart;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStart();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStart : virtual public AOMEvent {
    DECLARE_META_EVENT(evStart)
};
//#]
#endif // _OMINSTRUMENT

//## event evKrokRuchu()
class evKrokRuchu : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKrokRuchu;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKrokRuchu();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKrokRuchu : virtual public AOMEvent {
    DECLARE_META_EVENT(evKrokRuchu)
};
//#]
#endif // _OMINSTRUMENT

//## event evSprawdzKolizje()
class evSprawdzKolizje : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevSprawdzKolizje;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evSprawdzKolizje();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevSprawdzKolizje : virtual public AOMEvent {
    DECLARE_META_EVENT(evSprawdzKolizje)
};
//#]
#endif // _OMINSTRUMENT

//## event evKolizja()
class evKolizja : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKolizja;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKolizja();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKolizja : virtual public AOMEvent {
    DECLARE_META_EVENT(evKolizja)
};
//#]
#endif // _OMINSTRUMENT

//## event eventmessage_0()
class eventmessage_0 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedeventmessage_0;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    eventmessage_0();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedeventmessage_0 : virtual public AOMEvent {
    DECLARE_META_EVENT(eventmessage_0)
};
//#]
#endif // _OMINSTRUMENT

//## event evCzerwone()
class evCzerwone : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevCzerwone;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evCzerwone();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevCzerwone : virtual public AOMEvent {
    DECLARE_META_EVENT(evCzerwone)
};
//#]
#endif // _OMINSTRUMENT

//## event evSwiec()
class evSwiec : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevSwiec;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evSwiec();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevSwiec : virtual public AOMEvent {
    DECLARE_META_EVENT(evSwiec)
};
//#]
#endif // _OMINSTRUMENT

//## event evAwaria()
class evAwaria : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevAwaria;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evAwaria();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevAwaria : virtual public AOMEvent {
    DECLARE_META_EVENT(evAwaria)
};
//#]
#endif // _OMINSTRUMENT

//## event evNaruszenie()
class evNaruszenie : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevNaruszenie;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evNaruszenie();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevNaruszenie : virtual public AOMEvent {
    DECLARE_META_EVENT(evNaruszenie)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/
