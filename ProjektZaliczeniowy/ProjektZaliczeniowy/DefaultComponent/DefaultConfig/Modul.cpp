/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Modul
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Modul.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Modul.h"
//#[ ignore
#define Default_Modul_Modul_SERIALIZE OM_NO_OP

#define Default_Modul_ZapiszUstawienia_SERIALIZE aomsmethod->addAttribute("nastawy", UNKNOWN2STRING(nastawy));

#define Default_Modul_czytajUstawienia_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Modul
Modul::Modul() {
    NOTIFY_CONSTRUCTOR(Modul, Modul(), 0, Default_Modul_Modul_SERIALIZE);
}

Modul::~Modul() {
    NOTIFY_DESTRUCTOR(~Modul, true);
}

int Modul::getId() const {
    return id;
}

void Modul::setId(int p_id) {
    id = p_id;
}

std::string Modul::getNazwa() const {
    return nazwa;
}

void Modul::setNazwa(std::string p_nazwa) {
    nazwa = p_nazwa;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedModul::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("nazwa", UNKNOWN2STRING(myReal->nazwa));
    aomsAttributes->addAttribute("id", x2String(myReal->id));
}
//#]

IMPLEMENT_META_P(Modul, Default, Default, false, OMAnimatedModul)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Modul.cpp
*********************************************************************/
