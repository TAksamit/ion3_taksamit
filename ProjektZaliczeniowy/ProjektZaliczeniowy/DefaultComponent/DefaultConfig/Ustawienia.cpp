/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Ustawienia
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Ustawienia.h"
//## link itsWrzeciono
#include "Wrzeciono.h"
//#[ ignore
#define Default_Ustawienia_Ustawienia_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Ustawienia
Ustawienia::Ustawienia() {
    NOTIFY_CONSTRUCTOR(Ustawienia, Ustawienia(), 0, Default_Ustawienia_Ustawienia_SERIALIZE);
    itsWrzeciono = NULL;
}

Ustawienia::~Ustawienia() {
    NOTIFY_DESTRUCTOR(~Ustawienia, true);
    cleanUpRelations();
}

Wrzeciono* Ustawienia::getItsWrzeciono() const {
    return itsWrzeciono;
}

void Ustawienia::setItsWrzeciono(Wrzeciono* p_Wrzeciono) {
    _setItsWrzeciono(p_Wrzeciono);
}

void Ustawienia::cleanUpRelations() {
    if(itsWrzeciono != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsWrzeciono");
            itsWrzeciono = NULL;
        }
}

void Ustawienia::__setItsWrzeciono(Wrzeciono* p_Wrzeciono) {
    itsWrzeciono = p_Wrzeciono;
    if(p_Wrzeciono != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsWrzeciono", p_Wrzeciono, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsWrzeciono");
        }
}

void Ustawienia::_setItsWrzeciono(Wrzeciono* p_Wrzeciono) {
    __setItsWrzeciono(p_Wrzeciono);
}

void Ustawienia::_clearItsWrzeciono() {
    NOTIFY_RELATION_CLEARED("itsWrzeciono");
    itsWrzeciono = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedUstawienia::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsWrzeciono", false, true);
    if(myReal->itsWrzeciono)
        {
            aomsRelations->ADD_ITEM(myReal->itsWrzeciono);
        }
}
//#]

IMPLEMENT_META_P(Ustawienia, Default, Default, false, OMAnimatedUstawienia)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Ustawienia.cpp
*********************************************************************/
