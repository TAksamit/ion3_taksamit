/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: UtrzymanieRuchu
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/UtrzymanieRuchu.h
*********************************************************************/

#ifndef UtrzymanieRuchu_H
#define UtrzymanieRuchu_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## actor UtrzymanieRuchu
#include "Operator.h"
//## package Default

//## actor UtrzymanieRuchu
class UtrzymanieRuchu : public Operator {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedUtrzymanieRuchu;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    UtrzymanieRuchu();
    
    //## auto_generated
    ~UtrzymanieRuchu();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedUtrzymanieRuchu : public OMAnimatedOperator {
    DECLARE_META(UtrzymanieRuchu, OMAnimatedUtrzymanieRuchu)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/UtrzymanieRuchu.h
*********************************************************************/
