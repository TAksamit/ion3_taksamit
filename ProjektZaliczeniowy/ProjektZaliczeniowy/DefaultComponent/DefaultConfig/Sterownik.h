/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Sterownik
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/

#ifndef Sterownik_H
#define Sterownik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## link itsDetekcjaKolizji
#include "DetekcjaKolizji.h"
//## link itsDetektorNaruszeniaBezpieczenstwa
#include "DetektorNaruszeniaBezpieczenstwa.h"
//## link itsDetektorPoprawnosciDanych
#include "DetektorPoprawnosciDanych.h"
//## link itsEnkoderPozycjiWrzeciona
#include "EnkoderPozycjiWrzeciona.h"
//## link itsNapedPozycjiWrzeciona_1
#include "NapedPozycjiWrzeciona.h"
//## link itsNapedWrzeciona_1
#include "NapedWrzeciona.h"
//## link itsPanel
#include "Panel.h"
//## link itsSygnalizacja
#include "Sygnalizacja.h"
//## link itsWrzeciono
class Wrzeciono;

//## package Default

//## class Sterownik
class Sterownik : public OMReactive {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSterownik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Sterownik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Sterownik();
    
    ////    Operations    ////
    
    //## operation UstawStan(Stany)
    void UstawStan(const Stany& stan);
    
    //## operation message_0()
    void message_0();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getStan() const;
    
    //## auto_generated
    void setStan(int p_stan);
    
    //## auto_generated
    Stany getStan0() const;
    
    //## auto_generated
    void setStan0(Stany p_stan0);
    
    //## auto_generated
    const bool getSygnalizacja() const;
    
    //## auto_generated
    DetekcjaKolizji* getItsDetekcjaKolizji() const;
    
    //## auto_generated
    DetektorNaruszeniaBezpieczenstwa* getItsDetektorNaruszeniaBezpieczenstwa() const;
    
    //## auto_generated
    DetektorPoprawnosciDanych* getItsDetektorPoprawnosciDanych() const;
    
    //## auto_generated
    EnkoderPozycjiWrzeciona* getItsEnkoderPozycjiWrzeciona() const;
    
    //## auto_generated
    NapedPozycjiWrzeciona* getItsNapedPozycjiWrzeciona() const;
    
    //## auto_generated
    void setItsNapedPozycjiWrzeciona(NapedPozycjiWrzeciona* p_NapedPozycjiWrzeciona);
    
    //## auto_generated
    NapedPozycjiWrzeciona* getItsNapedPozycjiWrzeciona_1() const;
    
    //## auto_generated
    NapedWrzeciona* getItsNapedWrzeciona() const;
    
    //## auto_generated
    void setItsNapedWrzeciona(NapedWrzeciona* p_NapedWrzeciona);
    
    //## auto_generated
    NapedWrzeciona* getItsNapedWrzeciona_1() const;
    
    //## auto_generated
    Panel* getItsPanel() const;
    
    //## auto_generated
    Sygnalizacja* getItsSygnalizacja() const;
    
    //## auto_generated
    Wrzeciono* getItsWrzeciono() const;
    
    //## auto_generated
    void setItsWrzeciono(Wrzeciono* p_Wrzeciono);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Attributes    ////
    
    int stan;		//## attribute stan
    
    Stany stan0;		//## attribute stan0
    
    const bool sygnalizacja;		//## attribute sygnalizacja
    
    ////    Relations and components    ////
    
    DetekcjaKolizji itsDetekcjaKolizji;		//## link itsDetekcjaKolizji
    
    DetektorNaruszeniaBezpieczenstwa itsDetektorNaruszeniaBezpieczenstwa;		//## link itsDetektorNaruszeniaBezpieczenstwa
    
    DetektorPoprawnosciDanych itsDetektorPoprawnosciDanych;		//## link itsDetektorPoprawnosciDanych
    
    EnkoderPozycjiWrzeciona itsEnkoderPozycjiWrzeciona;		//## link itsEnkoderPozycjiWrzeciona
    
    NapedPozycjiWrzeciona* itsNapedPozycjiWrzeciona;		//## link itsNapedPozycjiWrzeciona
    
    NapedPozycjiWrzeciona itsNapedPozycjiWrzeciona_1;		//## link itsNapedPozycjiWrzeciona_1
    
    NapedWrzeciona* itsNapedWrzeciona;		//## link itsNapedWrzeciona
    
    NapedWrzeciona itsNapedWrzeciona_1;		//## link itsNapedWrzeciona_1
    
    Panel itsPanel;		//## link itsPanel
    
    Sygnalizacja itsSygnalizacja;		//## link itsSygnalizacja
    
    Wrzeciono* itsWrzeciono;		//## link itsWrzeciono
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsNapedPozycjiWrzeciona(NapedPozycjiWrzeciona* p_NapedPozycjiWrzeciona);
    
    //## auto_generated
    void _setItsNapedPozycjiWrzeciona(NapedPozycjiWrzeciona* p_NapedPozycjiWrzeciona);
    
    //## auto_generated
    void _clearItsNapedPozycjiWrzeciona();
    
    //## auto_generated
    void __setItsNapedWrzeciona(NapedWrzeciona* p_NapedWrzeciona);
    
    //## auto_generated
    void _setItsNapedWrzeciona(NapedWrzeciona* p_NapedWrzeciona);
    
    //## auto_generated
    void _clearItsNapedWrzeciona();
    
    //## auto_generated
    void __setItsWrzeciono(Wrzeciono* p_Wrzeciono);
    
    //## auto_generated
    void _setItsWrzeciono(Wrzeciono* p_Wrzeciono);
    
    //## auto_generated
    void _clearItsWrzeciono();
    
    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSterownik : virtual public AOMInstance {
    DECLARE_META(Sterownik, OMAnimatedSterownik)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Sterownik.h
*********************************************************************/
