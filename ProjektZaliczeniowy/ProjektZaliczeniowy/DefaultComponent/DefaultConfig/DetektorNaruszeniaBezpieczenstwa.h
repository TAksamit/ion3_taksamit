/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: DetektorNaruszeniaBezpieczenstwa
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/DetektorNaruszeniaBezpieczenstwa.h
*********************************************************************/

#ifndef DetektorNaruszeniaBezpieczenstwa_H
#define DetektorNaruszeniaBezpieczenstwa_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class DetektorNaruszeniaBezpieczenstwa
#include "Modul.h"
//## link itsSterownik
class Sterownik;

//## package Default

//## class DetektorNaruszeniaBezpieczenstwa
class DetektorNaruszeniaBezpieczenstwa : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDetektorNaruszeniaBezpieczenstwa;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    DetektorNaruszeniaBezpieczenstwa(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~DetektorNaruszeniaBezpieczenstwa();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Sterownik* getItsSterownik() const;
    
    //## auto_generated
    void setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Sterownik* itsSterownik;		//## link itsSterownik
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _setItsSterownik(Sterownik* p_Sterownik);
    
    //## auto_generated
    void _clearItsSterownik();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedDetektorNaruszeniaBezpieczenstwa : public OMAnimatedModul {
    DECLARE_META(DetektorNaruszeniaBezpieczenstwa, OMAnimatedDetektorNaruszeniaBezpieczenstwa)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/DetektorNaruszeniaBezpieczenstwa.h
*********************************************************************/
