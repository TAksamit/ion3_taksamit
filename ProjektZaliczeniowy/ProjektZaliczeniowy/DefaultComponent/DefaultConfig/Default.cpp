/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## auto_generated
#include "DetekcjaKolizji.h"
//## auto_generated
#include "DetektorNaruszeniaBezpieczenstwa.h"
//## auto_generated
#include "DetektorPoprawnosciDanych.h"
//## auto_generated
#include "EnkoderPozycjiWrzeciona.h"
//## auto_generated
#include "Modul.h"
//## auto_generated
#include "NapedPozycjiWrzeciona.h"
//## auto_generated
#include "NapedWrzeciona.h"
//## auto_generated
#include "Panel.h"
//## auto_generated
#include "Sterownik.h"
//## auto_generated
#include "Sygnalizacja.h"
//## auto_generated
#include "Ustawienia.h"
//## auto_generated
#include "Wrzeciono.h"
//#[ ignore
#define evImpuls_SERIALIZE OM_NO_OP

#define evImpuls_UNSERIALIZE OM_NO_OP

#define evImpuls_CONSTRUCTOR evImpuls()

#define evValidate_SERIALIZE OM_NO_OP

#define evValidate_UNSERIALIZE OM_NO_OP

#define evValidate_CONSTRUCTOR evValidate()

#define evDekoduj_SERIALIZE OM_NO_OP

#define evDekoduj_UNSERIALIZE OM_NO_OP

#define evDekoduj_CONSTRUCTOR evDekoduj()

#define evStop_SERIALIZE OM_NO_OP

#define evStop_UNSERIALIZE OM_NO_OP

#define evStop_CONSTRUCTOR evStop()

#define evStart_SERIALIZE OM_NO_OP

#define evStart_UNSERIALIZE OM_NO_OP

#define evStart_CONSTRUCTOR evStart()

#define evKrokRuchu_SERIALIZE OM_NO_OP

#define evKrokRuchu_UNSERIALIZE OM_NO_OP

#define evKrokRuchu_CONSTRUCTOR evKrokRuchu()

#define evSprawdzKolizje_SERIALIZE OM_NO_OP

#define evSprawdzKolizje_UNSERIALIZE OM_NO_OP

#define evSprawdzKolizje_CONSTRUCTOR evSprawdzKolizje()

#define evKolizja_SERIALIZE OM_NO_OP

#define evKolizja_UNSERIALIZE OM_NO_OP

#define evKolizja_CONSTRUCTOR evKolizja()

#define eventmessage_0_SERIALIZE OM_NO_OP

#define eventmessage_0_UNSERIALIZE OM_NO_OP

#define eventmessage_0_CONSTRUCTOR eventmessage_0()

#define evCzerwone_SERIALIZE OM_NO_OP

#define evCzerwone_UNSERIALIZE OM_NO_OP

#define evCzerwone_CONSTRUCTOR evCzerwone()

#define evSwiec_SERIALIZE OM_NO_OP

#define evSwiec_UNSERIALIZE OM_NO_OP

#define evSwiec_CONSTRUCTOR evSwiec()

#define evAwaria_SERIALIZE OM_NO_OP

#define evAwaria_UNSERIALIZE OM_NO_OP

#define evAwaria_CONSTRUCTOR evAwaria()

#define evNaruszenie_SERIALIZE OM_NO_OP

#define evNaruszenie_UNSERIALIZE OM_NO_OP

#define evNaruszenie_CONSTRUCTOR evNaruszenie()
//#]

//## package Default


#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

IMPLEMENT_META_PACKAGE(Default, Default)

static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}
#endif // _OMINSTRUMENT

//## event evImpuls()
evImpuls::evImpuls() {
    NOTIFY_EVENT_CONSTRUCTOR(evImpuls)
    setId(evImpuls_Default_id);
}

bool evImpuls::isTypeOf(const short id) const {
    return (evImpuls_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evImpuls, Default, Default, evImpuls())

//## event evValidate()
evValidate::evValidate() {
    NOTIFY_EVENT_CONSTRUCTOR(evValidate)
    setId(evValidate_Default_id);
}

bool evValidate::isTypeOf(const short id) const {
    return (evValidate_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evValidate, Default, Default, evValidate())

//## event evDekoduj()
evDekoduj::evDekoduj() {
    NOTIFY_EVENT_CONSTRUCTOR(evDekoduj)
    setId(evDekoduj_Default_id);
}

bool evDekoduj::isTypeOf(const short id) const {
    return (evDekoduj_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evDekoduj, Default, Default, evDekoduj())

//## event evStop()
evStop::evStop() {
    NOTIFY_EVENT_CONSTRUCTOR(evStop)
    setId(evStop_Default_id);
}

bool evStop::isTypeOf(const short id) const {
    return (evStop_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStop, Default, Default, evStop())

//## event evStart()
evStart::evStart() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

bool evStart::isTypeOf(const short id) const {
    return (evStart_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart, Default, Default, evStart())

//## event evKrokRuchu()
evKrokRuchu::evKrokRuchu() {
    NOTIFY_EVENT_CONSTRUCTOR(evKrokRuchu)
    setId(evKrokRuchu_Default_id);
}

bool evKrokRuchu::isTypeOf(const short id) const {
    return (evKrokRuchu_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKrokRuchu, Default, Default, evKrokRuchu())

//## event evSprawdzKolizje()
evSprawdzKolizje::evSprawdzKolizje() {
    NOTIFY_EVENT_CONSTRUCTOR(evSprawdzKolizje)
    setId(evSprawdzKolizje_Default_id);
}

bool evSprawdzKolizje::isTypeOf(const short id) const {
    return (evSprawdzKolizje_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evSprawdzKolizje, Default, Default, evSprawdzKolizje())

//## event evKolizja()
evKolizja::evKolizja() {
    NOTIFY_EVENT_CONSTRUCTOR(evKolizja)
    setId(evKolizja_Default_id);
}

bool evKolizja::isTypeOf(const short id) const {
    return (evKolizja_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKolizja, Default, Default, evKolizja())

//## event eventmessage_0()
eventmessage_0::eventmessage_0() {
    NOTIFY_EVENT_CONSTRUCTOR(eventmessage_0)
    setId(eventmessage_0_Default_id);
}

bool eventmessage_0::isTypeOf(const short id) const {
    return (eventmessage_0_Default_id==id);
}

IMPLEMENT_META_EVENT_P(eventmessage_0, Default, Default, eventmessage_0())

//## event evCzerwone()
evCzerwone::evCzerwone() {
    NOTIFY_EVENT_CONSTRUCTOR(evCzerwone)
    setId(evCzerwone_Default_id);
}

bool evCzerwone::isTypeOf(const short id) const {
    return (evCzerwone_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evCzerwone, Default, Default, evCzerwone())

//## event evSwiec()
evSwiec::evSwiec() {
    NOTIFY_EVENT_CONSTRUCTOR(evSwiec)
    setId(evSwiec_Default_id);
}

bool evSwiec::isTypeOf(const short id) const {
    return (evSwiec_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evSwiec, Default, Default, evSwiec())

//## event evAwaria()
evAwaria::evAwaria() {
    NOTIFY_EVENT_CONSTRUCTOR(evAwaria)
    setId(evAwaria_Default_id);
}

bool evAwaria::isTypeOf(const short id) const {
    return (evAwaria_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evAwaria, Default, Default, evAwaria())

//## event evNaruszenie()
evNaruszenie::evNaruszenie() {
    NOTIFY_EVENT_CONSTRUCTOR(evNaruszenie)
    setId(evNaruszenie_Default_id);
}

bool evNaruszenie::isTypeOf(const short id) const {
    return (evNaruszenie_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evNaruszenie, Default, Default, evNaruszenie())

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/
