/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Panel
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Panel.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Panel.h"
//## link itsSterownik
#include "Sterownik.h"
//#[ ignore
#define Default_Panel_Panel_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Panel
Panel::Panel(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Panel, Panel(), 0, Default_Panel_Panel_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsSterownik = NULL;
}

Panel::~Panel() {
    NOTIFY_DESTRUCTOR(~Panel, false);
    cleanUpRelations();
}

int Panel::getImpulsOK() const {
    return impulsOK;
}

void Panel::setImpulsOK(int p_impulsOK) {
    impulsOK = p_impulsOK;
}

Sterownik* Panel::getItsSterownik() const {
    return itsSterownik;
}

void Panel::setItsSterownik(Sterownik* p_Sterownik) {
    _setItsSterownik(p_Sterownik);
}

bool Panel::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Panel::cleanUpRelations() {
    if(itsSterownik != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
            itsSterownik = NULL;
        }
}

void Panel::__setItsSterownik(Sterownik* p_Sterownik) {
    itsSterownik = p_Sterownik;
    if(p_Sterownik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSterownik", p_Sterownik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSterownik");
        }
}

void Panel::_setItsSterownik(Sterownik* p_Sterownik) {
    __setItsSterownik(p_Sterownik);
}

void Panel::_clearItsSterownik() {
    NOTIFY_RELATION_CLEARED("itsSterownik");
    itsSterownik = NULL;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPanel::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("impulsOK", x2String(myReal->impulsOK));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedPanel::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsSterownik", false, true);
    if(myReal->itsSterownik)
        {
            aomsRelations->ADD_ITEM(myReal->itsSterownik);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_REACTIVE_META_S_SIMPLE_P(Panel, Default, false, Modul, OMAnimatedModul, OMAnimatedPanel)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Panel.cpp
*********************************************************************/
